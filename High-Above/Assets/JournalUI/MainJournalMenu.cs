﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainJournalMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene("playtestplease", LoadSceneMode.Single);
        Time.timeScale = 1.0f;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }
    
}
