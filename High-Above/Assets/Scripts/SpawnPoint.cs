﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public GameObject spawnObject;

    public float repeatTime = 3f;

	// Use this for initialization
	void Start () {
        //Spawns the spawn object every x seconds, where x is repeatTime
        //InvokeRepeating("Spawn", 2f, repeatTime);

        Spawn();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void Spawn()
    {
        Instantiate(spawnObject, transform.position, Quaternion.identity);
    }
}
