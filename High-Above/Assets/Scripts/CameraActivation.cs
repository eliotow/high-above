﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActivation : MonoBehaviour
{
    public GameObject cameraHolder;
    public GameObject Camera;
    public GameObject CSpline;
    private float timer;
    public float beginTime = 18;
    public bool timerOn = false;
    // Start is called before the first frame update
    void Start()
    {
        timer = beginTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerOn & timer > 0)
        {
            timer -= 1 * Time.deltaTime;
            //print(timer);
        }
        if (timer <= 0)
        {
            timerOn = false;
            timer = beginTime;
            Camera.GetComponent<CameraMovement>().enabled = !Camera.GetComponent<CameraMovement>().enabled;
            Camera.transform.position = cameraHolder.transform.position;
            //Camera.GetComponent<CameraEndSet>().enabled = true;
            CSpline.SetActive(false);

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            CSpline.SetActive(true);
            if(timerOn == false)
            {
                timerOn = true;
            }
        }
    }
}
