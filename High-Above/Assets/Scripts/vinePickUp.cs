﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class vinePickUp : MonoBehaviour
{
    private SphereCollider spherey;
    private GameObject otherObject;
    public Transform parentTransform;
    private bool carryingSomething;
    private float timer;
    public GameObject dropLocation;
    public float rotationLock = 30;
    // Start is called before the first frame update
    void Start()
    {
        carryingSomething = false;
        spherey = this.GetComponent<SphereCollider>();
        parentTransform = this.GetComponentInParent<Vine>().Player;
    }

    // Update is called once per frame
    void Update()
    {
        dropLocation.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, rotationLock, rotationLock);
        timer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space) && otherObject != null && carryingSomething == false && timer >= .5) {
            carryingSomething = true;
            timer = 0;
            //otherObject.GetComponent<BugMovementScript>().canMove = false;
            if (otherObject.tag == "bug")
            {
                otherObject.GetComponent<BugMovementScript>().enabled = false;
                otherObject.GetComponent<CapsuleCollider>().enabled = false;
                otherObject.GetComponent<Rigidbody>().detectCollisions = false;
                //otherObject.transform.rotation = Quaternion.Euler(rotationLock, rotationLock, parentTransform.rotation.eulerAngles.z);
            }
            else if (otherObject.tag == "snail")
            {
                otherObject.GetComponent<SnailMovement>().enabled = false;
                otherObject.GetComponent<CapsuleCollider>().enabled = false;
                otherObject.GetComponent<Rigidbody>().detectCollisions = false;
                //otherObject.transform.rotation = Quaternion.Euler(rotationLock, rotationLock, parentTransform.rotation.eulerAngles.z);
            }
            else if (otherObject.tag == "mushroom")
            {

            }
            otherObject.transform.position = parentTransform.position;

        }
        if (Input.GetKeyDown(KeyCode.Space) && otherObject != null && carryingSomething == true && timer >= .5)
        {
           
            carryingSomething = false;
            timer = 0;
            if (otherObject.tag == "bug")
            {
                otherObject.GetComponent<BugMovementScript>().enabled = true;
                otherObject.GetComponent<CapsuleCollider>().enabled = true;
                otherObject.GetComponent<Rigidbody>().detectCollisions = true;
                otherObject.transform.position = dropLocation.transform.position;
            }
            else if (otherObject.tag == "snail")
            {
                otherObject.GetComponent<SnailMovement>().enabled = true;
                otherObject.GetComponent<CapsuleCollider>().enabled = true;
                otherObject.GetComponent<Rigidbody>().detectCollisions = true;
                otherObject.transform.position = dropLocation.transform.position;
            }
            //otherObject.transform.position = parentTransform.position;

        }
        if (otherObject != null && carryingSomething == true)
        {
            otherObject.transform.position = parentTransform.position;
        }
        spherey.transform.position = parentTransform.position;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "bug" || other.gameObject.tag == "snail" && carryingSomething == false) {
            otherObject = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == otherObject && carryingSomething == false) {
            Debug.Log("lost a bug");
            otherObject = null;
        }
    }

}
