﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

public class BugQueueManager : MonoBehaviour {
    public List<Transform> BugQueue = new List<Transform>();
    public float mindistance = 5;
    public float moveSpeed = 500;
    

    private float dis;
    private Transform curBug;
    private Transform prevBug;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
            BugLine();
        
        
	}

    public void BugLine()// function to make bugs move after one another in a line
    {
        /*if (Input.GetAxis("Horizontal") != 0)
        {
            BugQueue[0].Rotate(Vector3.up * rotationspeed * Time.deltaTime * Input.GetAxis("Horizontal"));
        }
        */
        for (int i = 1; i < BugQueue.Count; i++)
        {
            curBug = BugQueue[i];
            prevBug = BugQueue[i - 1];

            dis = Vector3.Distance(prevBug.position, curBug.position);

            Vector3 newpos = prevBug.position;

            //newpos.y = BodyParts[0].position.y;

            float T = Time.deltaTime * dis / mindistance * moveSpeed;

            if (T > 0.5f)
                T = 0.5f;
            curBug.position = Vector3.Slerp(curBug.position, newpos, T);
            curBug.rotation = Quaternion.Slerp(curBug.rotation, prevBug.rotation, T);


        }

    }

}
