﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{

	private int health = 3;
    

    // Use this for initialization
    void Awake()
    {

    }
    private void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
		//if (health <= 0)
			//Destroy (gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //...
            //Debug.Log("player triggered mushroom");
            
        }

        if (other.tag == "bug")
        {
            //tickSource.Play();
            //GetComponent<ParticleSystem>().enableEmission = true; // turn on spore when bug reaches mushroom

			StartCoroutine(ScaleOverTime(1)); //decrease scale continuously when bug reaches mushroom
            //Debug.Log("bug triggered mushroom");
        }

		if (other.tag == "caterpillar") 
		{

		}

		if (other.tag == "ants") 
		{
			health -= 1;
			StartCoroutine(ShrinkSize(1)); 
		}
        

    }


    IEnumerator ScaleOverTime(float time) //coroutine that causes object attached to this script to decrease in size over time and then destroys it
    {
        Vector3 originalScale = transform.localScale;
        Vector3 destinationScale = new Vector3(0f, 0f, 0f);

        float currentTime = -1.0f; //delay before starting to decrease size

        do
        {
            transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        Destroy(gameObject);

    }

	IEnumerator ShrinkSize(float time) //coroutine that causes object attached to this script to decrease in size over time and then destroys it
	{
		Vector3 originalScale = transform.localScale;
		Vector3 destinationScale = new Vector3(transform.localScale.x * .75f, transform.localScale.y *.75f, transform.localScale.z *.75f);

		float currentTime = -1.0f; //delay before starting to decrease size

		do
		{
			transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
			currentTime += Time.deltaTime;
			yield return null;
		} while (currentTime <= time);



	}

    void OnTriggerExit(Collider other)
    {
        if(other.tag == "player")
        {
            //...
        }
        
    }
}
