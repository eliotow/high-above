﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class JournalEvents : MonoBehaviour
{
    public bool isPlayerActive = false;
    public bool isBugFoodActive = false;
    public bool isSnailActive = false;
    public bool IconCanAppear = true;
    GameObject journal;

    public GameObject[] bugEntry;
    public GameObject[] bugEntry2;
    public GameObject[] snailEntry;
    public GameObject[] hiddenBugEntry;
    public GameObject alertIcon;
    public GameObject menuButton;

    public Image alertImage;
    //public SpriteRenderer spriteView;
    //public GameObject bug;
    bugFollow bugFollowScript;


    // Start is called before the first frame update
    void Start()
    {
        journal = GameObject.FindGameObjectWithTag("Journal");
        //bugFollowScript = bug.GetComponent<bugFollow>();
        bugEntry = GameObject.FindGameObjectsWithTag("BugEntry");
        bugEntry2 = GameObject.FindGameObjectsWithTag("BugEntry2");
        snailEntry = GameObject.FindGameObjectsWithTag("SnailEntry");
        alertIcon = GameObject.FindGameObjectWithTag("alertIcon");
        
        //spriteView = alertIcon.GetComponent<SpriteRenderer>();
        

        //hiddenBugEntry = GameObject.FindGameObjectsWithTag("HiddenBugEntry");
        deactivate(bugEntry/*, hiddenBugEntry*/);
        deactivate(bugEntry2/*, hiddenBugEntry*/);
        deactivate(snailEntry/*, hiddenBugEntry*/);
        journal.gameObject.SetActive(false);
        alertIcon.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {


        if (isPlayerActive)
        {
            //print("WOOOO");
            activate(bugEntry/*, hiddenBugEntry*/);
            if (IconCanAppear == true) {
                alertImage.enabled = true;
                //spriteView.enabled = true;
                IconCanAppear = false;
               // Debug.Log("Spriteview is" + spriteView.enabled);
            }
            
        }

        if (isBugFoodActive)
        {
            //print("WOOOO");
            activate(bugEntry2/*, hiddenBugEntry*/);
            if (IconCanAppear == true)
           {
                // spriteView.enabled = true;
                alertImage.enabled = true;
                IconCanAppear = false;
               // Debug.Log("Spriteview is" + spriteView.enabled);
            }
        }

        if (isSnailActive)
        {
            //print("WOOOO");
           if (IconCanAppear == true)
            {
                //spriteView.enabled = true;
                IconCanAppear = false;
                alertImage.enabled = true;
                //Debug.Log("Spriteview is" + spriteView.enabled);
            }
            activate(snailEntry/*, hiddenBugEntry*/);
        }
      
    }


        void activate(GameObject[] array/*, GameObject[] hidden*/)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i].SetActive(true);
                //hidden[i].SetActive(false);
            }
        }

        void deactivate(GameObject[] array/*, GameObject[] hidden*/)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i].SetActive(false);
                //hidden[i].SetActive(true);
            }
        }
   public void disableIcon() {
        if (alertImage.enabled == true) {
            alertImage.enabled = false;
           // IconCanAppear = true;
        }
        Debug.Log("Spriteview is" + alertImage.enabled);
    }
    //The above function is called when the pause button is pressed and disables the icon
}
