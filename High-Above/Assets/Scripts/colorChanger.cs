﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colorChanger : MonoBehaviour
{

    //mushrooms
    public Material mR_Material;
    public Material mG_Material;
    public Material mB_Material;

    //beetles
    public Material bR_Material;
    public Material bG_Material;
    public Material bB_Material;
    //Renderer m_Renderer;

    private Material objectCurrentMaterial;
    public Material activeMaterial;
    private Material storage;

    void Start()
    {
        int random = Random.Range(0, 3);
        if (random == 0)
        {
            activeMaterial = mR_Material;
            
        }
        if (random == 1)
        {
            activeMaterial = mG_Material;
        }
        if (random == 2)
        {
            activeMaterial = mB_Material;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {//if collide with mushroom, change its texture

        if (collision.gameObject.CompareTag("mushroom"))
        {
            objectCurrentMaterial = collision.gameObject.GetComponent<Renderer>().material;
            //activeMaterial = 
            if (activeMaterial != objectCurrentMaterial)
            {
                storage = objectCurrentMaterial;
                collision.gameObject.GetComponent<Renderer>().material = activeMaterial;
                activeMaterial = storage;
            }

        }
        if (collision.gameObject.CompareTag("bug"))
        {
            objectCurrentMaterial = collision.gameObject.GetComponent<Renderer>().material;



            if (objectCurrentMaterial =  bR_Material)
            {
                activeMaterial = mR_Material;
            }
            if (objectCurrentMaterial= bG_Material)
            {
                activeMaterial = mG_Material;
            }
            if (objectCurrentMaterial = bB_Material)
            {
                activeMaterial = mB_Material;
            }
        }
    }
}


    
        

    
