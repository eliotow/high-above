﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SplineMesh;

using System.Collections.ObjectModel;

using UnityEngine.Events;



public class VineWork : MonoBehaviour
{
    public float speed;

    private Rigidbody rb;




    void Start()
    {

        rb = GetComponent<Rigidbody>();

    }

    void FixedUpdate()
    {

        float moveHorizontal = Input.GetAxis("Horizontal");

        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);

        rb.AddForce(movement * speed);
    }

    void Update()
    {
        if (Input.GetKeyDown("space"))
        {

            //Debug.Log("space pressed");
            GameObject.Find("Pipe").GetComponent<Spline>().AddNode(new SplineNode(new Vector3(transform.position.x, transform.position.y, 0), new Vector3(transform.position.x, transform.position.y, 0)));
           

        }
    }
}
