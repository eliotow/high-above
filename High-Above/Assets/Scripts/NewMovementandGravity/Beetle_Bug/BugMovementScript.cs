﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

public class BugMovementScript : MonoBehaviour
{
    // for Bug Lineup
    /*public  List<Transform> BugQueue = new List<Transform>();
    public float mindistance = 5;

    private float dis;
    private Transform curBug;
    private Transform prevBug;
   */
    BugQueueManager BugQueueManager;
    PlayerMovementScript PlayerMovementScript;

    //Move Speeds
    public float moveSpeed = 1.2f;

    private float startingMoveSpeed = 1.2f;

    public float rotationspeed = 50;

    //public float boostSpeed = 0.3f;

    //Timer for speed boost
   // private float boostTimer = 1.5f;

    //References
    public Transform target;

    public Vector3 target2;

    GameObject Player;

    JournalEvents journal;

    //Other Value Trackers
    public float growthCounter = 1;


    //Booleans
    public bool isFollowingPlayer;

    private bool moveable;

    private bool foundMushroom;

    public bool isFollowingSnail;

    public bool neverDoneSlow; //weird name, but used so bugs only slow the vine once while following it

    //public bool neverDoneReset;

    //public bool neverDoneDecay;

    //private bool startBoostTimer;

    private bool foundSap;

    public bool isCarrying;



    //Tree Gravity info
    public Transform cylinderTransform;

    public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder



    void Start()
    {
        journal = GameObject.Find("EventHandle").GetComponent<JournalEvents>();


        target = GameObject.FindWithTag("Player").transform;
        cylinderTransform = GameObject.FindWithTag("tree").transform;

        Player = GameObject.FindWithTag("Player");
        PlayerMovementScript = Player.GetComponent<PlayerMovementScript>();

        BugQueueManager = Player.GetComponent<BugQueueManager>();



        moveable = true;
        isFollowingPlayer = false;
        isFollowingSnail = false;
        foundMushroom = false;
        neverDoneSlow = true;
        //neverDoneReset = false;
        //neverDoneDecay = true;
        //startBoostTimer = false;
        foundSap = false;
        isCarrying = false;



    }

    void Update()
    {
        /*if (neverDoneSlow)
        {
            if (isFollowingPlayer == true)
            {
                PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer - 0.1f;
                neverDoneSlow = false;//this enables the bug to slow the player movement, but only once while following (this will be reset when bug follows snail, so that when it follows back to player the slow still applies)
                neverDoneReset = true;


            }

        }*/

        if (isFollowingPlayer == true || isFollowingSnail == true)
        {
            GetComponentInChildren<Animation>().enabled = true;
        }

        if(isFollowingPlayer == false && isFollowingSnail == false)
        {
            
            GetComponentInChildren<Animation>().enabled = false;
        }

        /*if (neverDoneReset)
        {
            if(isFollowingPlayer == false)
            {
                PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer + 0.1f;
                StartCoroutine(speedBoost());
                
                neverDoneReset = false; // this does the opposite of the above; allows bugs to give back movement speed once they are sent off the vine
            }
        }

        if (startBoostTimer)
        {
            boostTimer -= Time.deltaTime;
        }

        if (neverDoneDecay)
        {
            if (boostTimer <= 0)
            {
                PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer - boostSpeed;
                neverDoneDecay = false;
            }
        }*/

    }

    void FixedUpdate()
    {
        
        
        //GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.TransformDirection(moveDirection) * moveSpeed * Time.deltaTime);
        if (isFollowingPlayer == true )
        {


            
           //transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);

            isFollowingSnail = false; //turns off speed modification

            //moveSpeed = startingMoveSpeed;
                    
            Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);// these together work to make the bug look at the player when following
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position, gravityUp);


            
            
            



        }
        
        if(isFollowingPlayer == false)
        {
			BugQueueManager.BugQueue.Remove(transform);
        }


        /*if(isFollowingPlayer == true & BugQueueManager.BugQueue.Contains(transform))
        {
            BugLine();
            
        }*/
    
        if (foundMushroom == true)
        {
          transform.position = Vector3.MoveTowards(transform.position, target2, moveSpeed * Time.deltaTime); //once the bug finds a mushroom, it will go to it
            StartCoroutine(BugShroomReset());//calls a routine that makes foundMushroom false after 1.5 seconds; thought this was a fix but it wasn't, might be vestigial now
            

        }

        if (isFollowingSnail == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            moveSpeed = 0.5f;

            Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position, gravityUp);
            //neverDoneSlow = true; //resets this boolean so the bug can slow the vine down again if it reaattaches to the vine from a snail
        }

        if (foundSap == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);

            Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position, gravityUp);
            //neverDoneSlow = true;

            StartCoroutine (GrowOverTime(1));

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && moveable == true) // when bug zone finds player, make isFollowing true (follow the player)
        {
            if(!BugQueueManager.BugQueue.Contains(other.transform))
            {
                BugQueueManager.BugQueue.Add(other.transform);
            }
            target = other.gameObject.transform/*.GetChild(0).transform*/;
            isFollowingPlayer = true;
            BugQueueManager.BugQueue.Add(transform);

            journal.isPlayerActive = true;
            

        }

        if (other.tag == "mushroom" && isFollowingPlayer == true)
        {
            
            foundMushroom = true;
            

           isFollowingPlayer = false; //when bug zone finds mushroom, stop moving the bug
           //moveable = false; //makes bug unable to be picked up again by player

           target2 = other.gameObject.transform.position; //gets the position of the found mushroom

            journal.isBugFoodActive = true;
        }

        if (other.tag == "snail" && moveable == true)
        {
            isFollowingPlayer = false; // allows speed to be modified
            target = other.gameObject.transform;


            isFollowingSnail = true;
            

        }

        if (other.tag != "sap")
        {
            foundSap = false;
        }

        if (other.tag == "sap" && moveable == true)
        {
            
            isFollowingPlayer = false;
            target = other.gameObject.transform;
            foundSap = true;
        }




    }

    IEnumerator GrowOverTime(float time)
    {
        Vector3 originalScale = transform.localScale;
        Vector3 destinationScale = new Vector3(transform.localScale.x * 1.25f, transform.localScale.y * 1.25f, transform.localScale.z * 1.25f);

        float currentTime = -1.0f;

        do
        {
            transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= time);

        foundSap = false;
    }

    private void OnCollisionEnter(Collision collision)
    {


        if (collision.gameObject.tag == "Player") //interaction with player (add conditions for bug size/color/speed)
        {
            //isFollowing = false;
            //moveable = false;
            


        }


    }

    IEnumerator BugShroomReset()
    {
        yield return new WaitForSeconds(1.5f);
        foundMushroom = false;
    }

   /* IEnumerator speedBoost()
    {
        PlayerMovementScript.currentMoveSpeedPlayer = PlayerMovementScript.currentMoveSpeedPlayer + boostSpeed;
        startBoostTimer = true;
        yield return new WaitForSeconds(0.1f);

    }*/

    public void BugLine()// function to make bugs move after one another in a line
    {
        /*if (Input.GetAxis("Horizontal") != 0)
        {
            BugQueue[0].Rotate(Vector3.up * rotationspeed * Time.deltaTime * Input.GetAxis("Horizontal"));
        }

        for (int i = 1; i < BugQueue.Count; i++)
        {
            curBug = BugQueue[i];
            prevBug = BugQueue[i - 1];

            dis = Vector3.Distance(prevBug.position, curBug.position);

            Vector3 newpos = prevBug.position;

            //newpos.y = BodyParts[0].position.y;

            float T = Time.deltaTime * dis / mindistance * moveSpeed;

            if (T > 0.5f)
                T = 0.5f;
            curBug.position = Vector3.Slerp(curBug.position, newpos, T);
            curBug.rotation = Quaternion.Slerp(curBug.rotation, prevBug.rotation, T);


        }
        */

    }

}
