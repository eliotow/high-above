﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugGravityBody : MonoBehaviour {

    public TreeGravityScript attractorPlanet;
    private Transform characterTransform;
    public Transform target;
    

    void Start()
    {
        attractorPlanet = GameObject.FindWithTag("tree").GetComponent<TreeGravityScript>();
        characterTransform = GameObject.FindWithTag("tree").transform;

        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

        characterTransform = transform;

	
    }

    void Update()
    {

        if (GetComponent<BugMovementScript>().isFollowingPlayer == true)
        {
            // same thing can be called inside of BugMovementScript, not deleting cause scared to :))
        }
        
        

    }

    void FixedUpdate()
    {
        if (attractorPlanet)
        {
            attractorPlanet.AttractBug(characterTransform);


        }
        

    }
}
