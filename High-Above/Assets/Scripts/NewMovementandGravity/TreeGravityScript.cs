﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGravityScript : MonoBehaviour {

    public float gravity = -12;
    private Transform cylinderTransform;
    private Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder
    public Transform target;

    void Start()
    {
        cylinderTransform = transform;

        
    }

    public void Attract(Transform characterTransform)
    {
        //Vector3 gravityUp = (playerTransform.position - transform.position).normalized; // gravity vector for a sphere
        Vector3 gravityUp = Vector3.ProjectOnPlane(characterTransform.position - cylinderTransform.position, cylinderAxis); //gravity vector for our tree (cylinder)
        Vector3 localUp = characterTransform.up;

        characterTransform.GetComponent<Rigidbody>().AddForce(gravityUp * gravity);

        Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * characterTransform.rotation;
        characterTransform.rotation = Quaternion.Slerp(characterTransform.rotation, targetRotation, 50f * Time.deltaTime);
    }

    public void AttractBug(Transform characterTransform)
    {
        Vector3 gravityUp = Vector3.ProjectOnPlane(characterTransform.position - cylinderTransform.position, cylinderAxis);
        Vector3 localUp = characterTransform.up;

        characterTransform.GetComponent<Rigidbody>().AddForce(gravityUp * gravity);

        Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * characterTransform.rotation;
        characterTransform.rotation = Quaternion.Slerp(characterTransform.rotation, targetRotation, 50f * Time.deltaTime);
        //characterTransform.rotation = Quaternion.LookRotation(target.transform.position - characterTransform.position, gravityUp); //bugs will look at player when following, *moved to BugMovementScript



    }

    public void AttractSnail(Transform characterTransform)
    {
        Vector3 gravityUp = Vector3.ProjectOnPlane(characterTransform.position - cylinderTransform.position, cylinderAxis);
        Vector3 localUp = characterTransform.up;

        characterTransform.GetComponent<Rigidbody>().AddForce(gravityUp * gravity);

        Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * characterTransform.rotation;
        characterTransform.rotation = Quaternion.Slerp(characterTransform.rotation, targetRotation, 50f * Time.deltaTime);
        //characterTransform.rotation = Quaternion.LookRotation(target.transform.position - characterTransform.position, gravityUp); //bugs will look at player when following, *moved to BugMovementScript



    }

	public void AttractCaterpillar(Transform characterTransform)
	{
		Vector3 gravityUp = Vector3.ProjectOnPlane(characterTransform.position - cylinderTransform.position, cylinderAxis);
		Vector3 localUp = characterTransform.up;

		characterTransform.GetComponent<Rigidbody>().AddForce(gravityUp * gravity);

		Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * characterTransform.rotation;
		characterTransform.rotation = Quaternion.Slerp(characterTransform.rotation, targetRotation, 50f * Time.deltaTime);
		//characterTransform.rotation = Quaternion.LookRotation(target.transform.position - characterTransform.position, gravityUp); //bugs will look at player when following, *moved to BugMovementScript



	}

	public void AttractVine(Transform characterTransform)
	{
		//Vector3 gravityUp = (playerTransform.position - transform.position).normalized; // gravity vector for a sphere
		Vector3 gravityUp = Vector3.ProjectOnPlane(characterTransform.position - cylinderTransform.position, cylinderAxis); //gravity vector for our tree (cylinder)
		Vector3 localUp = characterTransform.up;

		characterTransform.GetComponent<Rigidbody>().AddForce(gravityUp * gravity);

		Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * characterTransform.rotation;
		characterTransform.rotation = Quaternion.Slerp(characterTransform.rotation, targetRotation, 50f * Time.deltaTime);
	}

	public void AttractAnts(Transform characterTransform)
	{
		//Vector3 gravityUp = (playerTransform.position - transform.position).normalized; // gravity vector for a sphere
		Vector3 gravityUp = Vector3.ProjectOnPlane(characterTransform.position - cylinderTransform.position, cylinderAxis); //gravity vector for our tree (cylinder)
		Vector3 localUp = characterTransform.up;

		characterTransform.GetComponent<Rigidbody>().AddForce(gravityUp * gravity);

		Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * characterTransform.rotation;
		characterTransform.rotation = Quaternion.Slerp(characterTransform.rotation, targetRotation, 50f * Time.deltaTime);
	}




}
