﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VineGravityBody : MonoBehaviour {

    public TreeGravityScript attractorPlanet;
    private Transform characterTransform;

    void Start()
    {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

        characterTransform = transform;
    }

    void FixedUpdate()
    {
        if (attractorPlanet)
        {
            attractorPlanet.Attract(characterTransform);
        }

        if (!Input.anyKey)
        {
            GetComponent<Rigidbody>().Sleep(); //This fixes the bodyparts flying off the tree when not moving (couldn't use kinematic, would cause bdoy parts to move into tree)
        }
    }
}
