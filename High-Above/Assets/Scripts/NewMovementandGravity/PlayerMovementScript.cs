﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour {
    protected Joystick joystick;

    public float currentMoveSpeedPlayer;

    private Vector3 moveDirection;
    void Start()
    {
        joystick = FindObjectOfType<Joystick>();
    }
    void Update()
    {
        moveDirection = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized; //for in-engine testing
       moveDirection = new Vector3(joystick.Horizontal * currentMoveSpeedPlayer, 0, joystick.Vertical * currentMoveSpeedPlayer); //for mobile ports

    }

    void FixedUpdate()
    {
       GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.TransformDirection(moveDirection) * currentMoveSpeedPlayer * Time.deltaTime);
    }
}
