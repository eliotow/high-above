﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeGravity : MonoBehaviour {

    public BeePusherScript attractorPlanet;
	private Transform characterTransform;
	public Transform target;
    public beeMovement beeMovement;

    //public Rigidbody rb;


	void Start()
	{
        beeMovement = GetComponent<beeMovement>();
        attractorPlanet = GameObject.FindWithTag("beePusher").GetComponent<BeePusherScript>();
       

        GetComponent<Rigidbody>().useGravity = false;


        characterTransform = transform;
	}

	void Update()
	{

	}

	void FixedUpdate()
	{
        if (attractorPlanet && beeMovement.wantToPushBee == true)
        {
            attractorPlanet.PushBee(characterTransform);


        }

        if (attractorPlanet)
        {
            attractorPlanet.BalanceBee(characterTransform);


        }

    }
}
