﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailGravityBody : MonoBehaviour {

    public TreeGravityScript attractorPlanet;
    private Transform characterTransform;
    //public Transform target;


    void Start()
    {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

        characterTransform = transform;
		attractorPlanet = GameObject.FindWithTag("tree").GetComponent<TreeGravityScript>();
    }

    void Update()
    {



    }

    void FixedUpdate()
    {
        if (attractorPlanet)
        {
            attractorPlanet.AttractSnail(characterTransform);


        }


    }
}
