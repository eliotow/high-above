﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnailMovement : MonoBehaviour {

    public float moveSpeed;

    public Transform cylinderTransform;

    public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder

    public float turnSpeed = 20; //For some reason this number is important, but changing it doesnt seem to do anything

    public float detectionDistance = 20; //how far raycast are sent out to detect objects for the snail

    JournalEvents journal;

    //public bool isBeingFollowed;

    void Start()
    {
        journal = GameObject.Find("EventHandle").GetComponent<JournalEvents>();

        transform.rotation = Random.rotation; //may only work for spheres if no constraints

		cylinderTransform = GameObject.FindWithTag("tree").transform;


    }

    void Update()
    {
   
    }

    void FixedUpdate()
    {

        transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);

        Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);


        //Raycasting to make snail rotate away if it detects player/vine
        RaycastHit hit;
        var forward = transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(transform.position, forward, out hit, detectionDistance))
        {


            if ( hit.collider.tag == "bug" || hit.collider.tag == "bodypart" || hit.collider.tag == "snailBoundary" ||hit.collider.tag == "branch")
                {

                transform.Rotate(Vector3.up * 5 * Time.deltaTime * turnSpeed); // the "5" here is how much it turns each detection, like a re-direct angle

            }

            if (hit.collider.tag == "Player" )
            {

                transform.Rotate(Vector3.up * 5 * Time.deltaTime * turnSpeed); // the "5" here is how much it turns each detection, like a re-direct angle
                journal.isSnailActive = true;
            }
            if (hit.collider.tag == "mushroom")
                {

                transform.Rotate(Vector3.up * 5 * Time.deltaTime * turnSpeed);

                }
            }      

    }

    private void OnTriggerEnter(Collider other)
    {

    }
}
