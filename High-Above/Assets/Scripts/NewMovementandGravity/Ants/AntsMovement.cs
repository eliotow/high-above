﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntsMovement : MonoBehaviour {

	public float moveSpeed;

	public Transform cylinderTransform;

	public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder

	public float turnSpeed = 20; //For some reason this number is important, but changing it doesnt seem to do anything

	public float detectionDistance = 20; //how far raycast are sent out to detect objects for the snail



	public Transform target;


	private bool foundBug;

	private bool alreadyDestroyed = false; //boolean used in OnCollision to make only 1 Ants get destroyed, rather than both

	private bool foundMushroom;

	private bool foundSap;

    private bool foundCaterpillar;



	public int swarmSize; //controls size of ants swarm for ants-to-ants interactions

	public Rigidbody antsPrefab;

	private float splitTimer; //counts down so newly instantiated ants from snail split, do not immediately merge with eachother

	private float sapTimer; //counts down so when sap is found, it won't keep triggering on the sap while it waits for animation




	// Use this for initialization
	void Start () {

		splitTimer = 2.0f; //timer for merge/split with snails, see IEnumerator(splitAnts()) for details
		sapTimer = 4.5f;

		foundBug = false;
		foundMushroom = false;
		foundSap = false;
        foundCaterpillar = false;

		cylinderTransform = GameObject.FindWithTag("tree").transform;

		swarmSize = 1;

		
	}
	
	// Update is called once per frame
	void Update () {

		splitTimer -= Time.deltaTime;
		sapTimer -= Time.deltaTime;

		Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);



		if (foundBug == false && foundMushroom == false && foundSap == false) 
		{
			transform.Translate (Vector3.forward * moveSpeed * Time.deltaTime);
			GetComponent<AntsGravityBody>().enabled = true;
		}

		if (foundBug == true) 
		{
			transform.position = Vector3.MoveTowards(transform.position, target.position, 3 * Time.deltaTime);
			GetComponent<AntsGravityBody> ().enabled = false; //allows it to travel to "attach point"
            GetComponentInChildren<Animation>().enabled = false;

            Vector3 gravityUp1 = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
            Vector3 localUp = transform.up;
            Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp1) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 50f * Time.deltaTime);

        }
        
        if (foundBug == false)
        {
            GetComponent<AntsGravityBody>().enabled = true;
            GetComponentInChildren<Animation>().enabled = true;
        }


		if (foundMushroom == true)
		{
			transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
			StartCoroutine (resetMovement ());
		}

		if (foundSap == true && sapTimer <= 0) 
		{
			StartCoroutine (buildBridge ());
		}
			
		
	}

	void FixedUpdate()
	{

		//Raycasting to make object rotate away if it detects something
		RaycastHit hit;
		var forward = transform.TransformDirection(Vector3.forward);
		if (Physics.Raycast(transform.position, forward, out hit, detectionDistance))
		{


			if (hit.collider.tag == "Player" || hit.collider.tag == "bodypart" || hit.collider.tag == "snailBoundary" || hit.collider.tag == "branch")

            {

				transform.Rotate(Vector3.up * 15 * Time.deltaTime * turnSpeed); // the "15" here is how much it turns each detection, like a re-direct angle

			}

			if (hit.collider.tag == "mushroom")
			{
				transform.Rotate (Vector3.up * 45 * Time.deltaTime * turnSpeed);
			}

			if (hit.collider.tag == "ants" && swarmSize >= 2) 
			{
				transform.Rotate (Vector3.up * 45 * Time.deltaTime * turnSpeed);
			}

		} 

		
	}

	private void OnTriggerEnter(Collider other)
	{

        if (other.tag == "bug" && other.gameObject.GetComponent<BugMovementScript>().isCarrying == false)
		{
			foundMushroom = false;


            other.gameObject.GetComponent<BugMovementScript>().isCarrying = true;


            foundBug = true;



			target = other.gameObject.transform.Find ("AttachPoint"); // finds the attach point on the beetle
            
			transform.parent = other.transform.parent;


		}

		if (other.tag == "mushroom") 
		{
			foundBug = false;

            

			//foundMushroom = true;



			//target = other.gameObject.transform;

		}

		if (other.tag == "sap") 
		{

			//called in OnCollisionEnter
		}

        if(other.tag == "caterpillar")
        {

            

        }

        if (other.tag == "Player" || other.tag == "bodypart" || other.tag == "player")
        {

            transform.Rotate(Vector3.up * 45 * Time.deltaTime * turnSpeed);

        }


    }

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "ants" && swarmSize < 2 && collision.gameObject.GetComponent<AntsMovement>().swarmSize < 2 && splitTimer < 0) 
		{
			splitTimer = 2.0f;
			swarmSize += 1;
			transform.localScale = transform.localScale * 2;

			if (!alreadyDestroyed) 
			{
				AntsMovement script = collision.gameObject.GetComponent<AntsMovement> ();

				if (script != null) 
				{
					script.alreadyDestroyed = true;
					Destroy (collision.gameObject);
				}

			}


			//Destroy (collision.gameObject);
		}

		if (collision.gameObject.tag == "snail" && swarmSize > 1) 
		{
			Instantiate (antsPrefab, transform.position, Random.rotation);
			transform.localScale = transform.localScale / 2;
			StartCoroutine (SplitAnts ());



		}

		if (collision.gameObject.tag == "snail") 
		{
			transform.Rotate (Vector3.up * 90 * Time.deltaTime * turnSpeed);

		}

		if (collision.gameObject.tag == "sap") 
		{
			foundBug = false;
			foundMushroom = false;

			target = collision.gameObject.transform;

			foundSap = true;
		}

        if (collision.gameObject.tag == "caterpillar")
        {
            foundCaterpillar = true;
            //other.gameObject.GetComponent<CaterpillarGravityBody>().enabled = false;
        }



    }

	IEnumerator SplitAnts()// also keeps ants from re-merging as soon as they split
	{
		yield return new WaitForSeconds (2.0f);
		swarmSize -= 1;
	}

	IEnumerator resetMovement() //reset all bools to false, so natural movement will continue (no target)
	{
		yield return new WaitForSeconds (2.0f); //gives time for animation and/or action

		//transform.Rotate (Vector3.up * 45 * Time.deltaTime * turnSpeed);

		foundMushroom = false;
	}

	IEnumerator buildBridge()
	{
		yield return new WaitForSeconds (1.5f);
		sapTimer = 4.5f; //room for animation to happen
		yield return new WaitForSeconds (1.5f);
		transform.position = target.position;
		foundSap = false;

	}
		
}
