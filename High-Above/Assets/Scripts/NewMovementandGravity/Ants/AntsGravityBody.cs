﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntsGravityBody : MonoBehaviour {

	public TreeGravityScript attractorPlanet;
	private Transform characterTransform;
	//public Transform target;


	void Start()
	{
		attractorPlanet = GameObject.FindWithTag("tree").GetComponent<TreeGravityScript>();
		characterTransform = GameObject.FindWithTag("tree").transform;

		GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

		characterTransform = transform;
	}

	void Update()
	{



	}

	void FixedUpdate()
	{
		if (attractorPlanet)
		{
			attractorPlanet.AttractAnts(characterTransform);


		}


	}
}
