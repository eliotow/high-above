﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatButt : MonoBehaviour {

	private Transform target;

	// Use this for initialization
	void Start () {

		target = transform.parent;
		
	}
	
	// Update is called once per frame
	void Update () {

		transform.position = new Vector3 (target.position.x - 0.5f, target.position.y - 0.1f, transform.position.z);
		
	}
}
