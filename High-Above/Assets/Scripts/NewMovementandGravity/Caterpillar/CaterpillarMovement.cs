﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaterpillarMovement : MonoBehaviour {

	public float moveSpeed;

	public Transform cylinderTransform;

	public Vector3 cylinderAxis = new Vector3(0, -4, 0); // **update if base tree is ever moved; must be any point along y axis of cylinder

	public float turnSpeed = 20; //For some reason this number is important, but changing it doesnt seem to do anything

	public float detectionDistance = 10; //how far raycast are sent out to detect objects for the snail



	public Transform target;


	private bool foundBug;

	private bool foundSap;

	private bool foundCaterpillar;

	private bool foundSnail;

	private bool foundMushroom;

    public bool foundBee;


	private bool sapEaten;






	void Start()
	{
		//transform.rotation = Random.rotation; //sends snail in random direction on spawn

		foundBug = false;
		foundSap = false;
		sapEaten = false;
		foundCaterpillar = false;
		foundSnail = false;
		foundMushroom = false;
        foundBee = false;

		cylinderTransform = GameObject.FindWithTag("tree").transform;
	}

	void Update()
	{
		//transform.Translate (Vector3.forward * moveSpeed * Time.deltaTime); (called in fixed update instead)
	}

	void FixedUpdate()
	{
		if (foundBug == false && foundSap == false && foundMushroom == false && foundBee == false) 
		{
			transform.Translate (Vector3.forward * moveSpeed * Time.deltaTime);
			GetComponent<CaterpillarGravityBody> ().enabled = true;
		}

		Vector3 gravityUp = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);


		//Raycasting to make object rotate away if it detects something
		RaycastHit hit;
		var forward = transform.TransformDirection(Vector3.forward);
		if (Physics.Raycast(transform.position, forward, out hit, detectionDistance))
		{


			if (hit.collider.tag == "Player" || hit.collider.tag == "bodypart" || hit.collider.tag == "snailBoundary")
			{

				transform.Rotate(Vector3.up * 15 * Time.deltaTime * turnSpeed); // the "15" here is how much it turns each detection, like a re-direct angle

			}

			if (hit.collider.tag == "mushroom")
			{
				transform.Rotate (Vector3.up * 45 * Time.deltaTime * turnSpeed);
			}
				
		}      

		if (foundBug == true) 
		{
			

            transform.position = Vector3.MoveTowards(transform.position, target.position, 3 * Time.deltaTime);
            GetComponent<CaterpillarGravityBody>().enabled = false; //allows it to travel to "attach point"
            //GetComponentInChildren<Animation>().enabled = false;

            Vector3 gravityUp1 = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
            Vector3 localUp = transform.up;
            Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp1) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 50f * Time.deltaTime);
        }

		if (foundSap == true)
		{
			transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
		}

		if (foundCaterpillar == true) 
		{
			transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
		}

		if (foundSnail == true) 
		{
            transform.position = Vector3.MoveTowards(transform.position, target.position, 3 * Time.deltaTime);
            GetComponent<CaterpillarGravityBody>().enabled = false; //allows it to travel to "attach point"
            //GetComponentInChildren<Animation>().enabled = false;

            Vector3 gravityUp1 = Vector3.ProjectOnPlane(transform.position - cylinderTransform.position, cylinderAxis);
            Vector3 localUp = transform.up;
            Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp1) * transform.rotation;
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 50f * Time.deltaTime);
        }

		if (foundMushroom == true) 
		{
			transform.position = Vector3.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
		}
        
		if (foundBug == false || foundSnail == false) 
		{
            GetComponent<AntsGravityBody>().enabled = true;
            //GetComponentInChildren<Animation>().enabled = true;
        }

        if (foundBee == true)
        {
            transform.position = target.position;
            GetComponent<MeshCollider>().enabled = false;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        
			

	}

	private void OnTriggerEnter(Collider other)
	{

		if (other.tag == "bug")
		{
			foundSap = false;
            foundBee = false;
            foundSnail = false;
            foundCaterpillar = false;

            target = other.gameObject.transform.Find ("AttachPoint"); // finds the attach point on the beetle

			foundBug = true;

			transform.parent = other.transform.parent;



		}
		if (other.tag == "Player" || other.tag == "bodyparts") 
		{
			transform.Rotate(Vector3.up * 15 * Time.deltaTime * turnSpeed);
		}

		if(other.tag == "sap")
		{
			foundBug = false;
            foundBee = false;
            foundSnail = false;
            foundCaterpillar = false;

            target = other.gameObject.transform;

			foundSap = true;

			StartCoroutine (catSapReset ());
		}

		if (other.tag == "caterpillar") 
		{
			foundBug = false;
			foundSap = false;
			foundSnail = false;
            foundBee = false;

			target = other.gameObject.transform;

			foundCaterpillar = true;

		}

		if (other.tag == "snail") 
		{
			foundBug = false;
			foundSap = false;
			foundCaterpillar = false;
            foundBee = false;

			target = other.gameObject.transform.Find ("AttachPoint");

			foundSnail = true;
		}

		if (other.tag == "mushroom") 
		{
			foundBug = false;
			foundSap = false;
			foundCaterpillar = false;
            foundBee = false;

			target = other.gameObject.transform;


			foundMushroom = true;
			StartCoroutine (catMushReset ());


		}

        if (other.tag == "bee")
        {
            foundBug = false;
            foundSap = false;
            foundCaterpillar = false;
            foundSnail = false;

            target = other.gameObject.transform.Find("AttachPoint");

            foundBee = true;
        }

        if (foundBee == true && other.CompareTag("mushroom"))
        {
            
        }

	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ants")
        {
            //GetComponent<CaterpillarGravityBody>().enabled = false;
            //target = collision.gameObject.transform.Find("CarryPoint");
        }

    }


        IEnumerator catSapReset()
	{
		yield return new WaitForSeconds(0.8f);
		foundSap = false;
	}

	IEnumerator catMushReset()
	{
		yield return new WaitForSeconds(3f);
		foundMushroom = false;
	}


}
