﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaterpillarGravityBody : MonoBehaviour {

	public TreeGravityScript attractorPlanet;
	private Transform characterTransform;
	//public Transform target;


	void Start()
	{
        attractorPlanet = GameObject.FindWithTag("tree").GetComponent<TreeGravityScript>();

        GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;

		characterTransform = transform;
	}

	void Update()
	{



	}

	void FixedUpdate()
	{
		if (attractorPlanet)
		{
			attractorPlanet.AttractCaterpillar(characterTransform);


		}


	}
}
