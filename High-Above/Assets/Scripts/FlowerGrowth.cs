﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerGrowth : MonoBehaviour {
    public float scalingFactor = 5;
    public float growTime = 10;
    private Vector3 initialScale;
    private Vector3 finalScale;
   
    private float scalingFramesLeft = 0;
    // Use this for initialization
    void Start () {
        scalingFramesLeft = growTime;
    }
	
	// Update is called once per frame
	void Update () {
        


        if (scalingFramesLeft > 0)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale * scalingFactor, Time.deltaTime * 10);
            scalingFramesLeft--;
        }
    }
}
