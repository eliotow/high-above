﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sap : MonoBehaviour {



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "caterpillar") 
		{
			StartCoroutine (ScaleOverTime (1));
		}

        if (other.tag == "bug" && other.gameObject.GetComponent<BugMovementScript>().growthCounter < 3)
        {
            StartCoroutine (ScaleOverTime(1));
            other.gameObject.GetComponent<BugMovementScript>().growthCounter += 1;
        }
	}

	IEnumerator ScaleOverTime(float time)
	{
		Vector3 originalScale = transform.localScale;
		Vector3 destinationScale = new Vector3 (0f, 0f, 0f);

		float currentTime = -1.0f;

		do {
			transform.localScale = Vector3.Lerp (originalScale, destinationScale, currentTime / time);
			currentTime += Time.deltaTime;
			yield return null;
		} while (currentTime <= time);

		Destroy (gameObject);
			
	}


}
