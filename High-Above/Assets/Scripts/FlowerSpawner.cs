﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerSpawner : MonoBehaviour
{
    // press c to make checkpoint, press v to return to checkpoint
    Vector3 flowerPoint;
    public float waitTime = 3f;
    float timer = 3f;
    public GameObject flowerPrefab;
    // Use this for initialization
    void Start()
    {
        flowerPoint = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer > 0)
        {
            timer -= 1 * Time.deltaTime;
            //print(timer);
        }
        if (Input.GetKeyDown("c") & timer <= 0)
        {
            SpawnFlower();

        }

      

    }

    public void SpawnFlower()
    {
        flowerPoint = transform.position;
        timer = waitTime;
        Instantiate(flowerPrefab, flowerPoint, transform.rotation);
    }
}
