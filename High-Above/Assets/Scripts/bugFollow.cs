﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bugFollow : MonoBehaviour {

	public GameObject player;
	public Transform player1; //reference for player transform
	public GameObject tree;
    JournalEvents journal;
	//public AudioSource tickSource;  //called in void Start (currently commented out)

	public float waitTime = 2f;
	public float bugSpeed = 2f;

	private float timer;	

	public bool isfollowing = false;

	private bool controllable = true;
	void Start()
	{
        //tickSource = GetComponent<AudioSource>();
        journal = GameObject.Find("EventHandle").GetComponent<JournalEvents>();
	}

	private void Update()
	{
		timer -= Time.deltaTime;
		if (timer <= 0f)
		{
			// CALL METHOD
			if (isfollowing)
			{
				StartCoroutine(MoveTo(player.transform.position, bugSpeed, tree.transform.position));


				var targetPosition = new Vector3(player1.transform.GetChild(0).transform.position.x, player1.transform.GetChild(0).transform.position.y, player1.transform.GetChild(0).transform.position.z); //code to restrain vector to x coordinate in next 3 lines
				targetPosition.x = transform.position.x; // keeps bugs x coordinate "looking at" player location as long as player1 reference is there
				transform.LookAt(targetPosition);
			}
			timer = waitTime;
		}
	}

	private IEnumerator MoveTo(Vector3 a, float speed, Vector3 offset)
	{
		float step = (speed / (transform.position - a).magnitude) * Time.fixedDeltaTime;
		float t = 0;
		while (t <= 1.0f)
		{
			t += step;
			transform.position = Vector3.Lerp(transform.position, a, t) - offset;
			yield return new WaitForFixedUpdate();
		}
		transform.position = a;
	}

	private void OnCollisionEnter(Collision collision)
	{


		if (collision.gameObject.name == "Player") //interaction with player (add conditions for bug size/color/speed)
		{



		}



	}


	private void OnTriggerEnter(Collider other)
	{
		if (other.name == "Player" && controllable == true)
		{
			isfollowing = true;

            journal.isPlayerActive = true;
            print("WAAA");
			//tickSource.Play(); //bug sound play while following
		}

		if (other.tag == "mushroom")
		{
			controllable = false;
			isfollowing = false;
            //journal.isOtherActive = true;

            
            StartCoroutine(MoveTo(other.transform.position, bugSpeed, tree.transform.position));



		}



	}
}