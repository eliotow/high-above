﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMenu : MonoBehaviour
{
    public GameObject joystick;
    public GameObject journalEntry;
    public bool isOpen = false;

   public void PauseGame()
    {
        Time.timeScale = 0.0f;
        joystick.SetActive(false);
        journalEntry.SetActive(true);
        isOpen = true;
    }
    public void ContinueGame()
    {
        Time.timeScale = 1.0f;
        joystick.SetActive(true);
        journalEntry.SetActive(false);
        isOpen = false;
    }

    public void ToggleGame()
    {
        if(isOpen == false)
        {
            PauseGame();
        }
        else
        {
            ContinueGame();
        }
        print(isOpen);
    }
}
