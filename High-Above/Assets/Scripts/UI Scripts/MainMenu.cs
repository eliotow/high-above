﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadSpringLevel ()
    {
        SceneManager.LoadScene("SpringLevel", LoadSceneMode.Single);
    }
    public void LoadWinterLevel()
    {
        SceneManager.LoadScene("WinterLevel", LoadSceneMode.Single);
    }
    public void LoadSummerLevel()
    {
        SceneManager.LoadScene("SummerLevel", LoadSceneMode.Single);
    }
    public void LoadFallLevel()
    {
        SceneManager.LoadScene("FallLevel", LoadSceneMode.Single);
    }

    public void QuitGame ()
    {
        Application.Quit();
    }
}
