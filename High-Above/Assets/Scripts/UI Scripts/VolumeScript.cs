﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeScript : MonoBehaviour {

    FMOD.Studio.Bus Master;
    FMOD.Studio.Bus Music;
    FMOD.Studio.Bus SFX;

    public float MasterVolume = 0.5f;
    public float MusicVolume = 0.5f;
    public float SFXVolume = 1f;

    public Slider MasterVolumeSlider;

    public void Awake()
    {
        Master = FMODUnity.RuntimeManager.GetBus("bus:/Master");
        Music = FMODUnity.RuntimeManager.GetBus("bus:/Master/Music");
        SFX = FMODUnity.RuntimeManager.GetBus("bus:/Master/SFX");
    }

    void Update()
    {
        Master.setVolume(MasterVolume);
        Music.setVolume(MusicVolume);
        SFX.setVolume(SFXVolume);
    }


    public void ChangeMasterVolume()
    {
        MasterVolume = MasterVolumeSlider.value;
        Debug.Log(MasterVolumeSlider.value);
    }



    public void MasterVolumeLevel(float newMasterVolume)
    {
        //MasterVolume = newMasterVolume;
        Master.setVolume(MasterVolume);
    }
    public void MusicVolumeLevel(float newMusicVolume)
    {
        //MusicVolume = newMusicVolume;
    }
    public void SFXVolumeLevel(float newSFXVolume)
    {
        //SFXVolume = newSFXVolume;
    }
}
