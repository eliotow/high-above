﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

	public GameObject point;

	public GameObject SplineRoot;

	private float pointCount = 0f;


	// Use this for initialization
	void Start () {
		
	}
	
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			Debug.Log ("space pressed");
			var splinePoint = Instantiate(point, transform.position, Quaternion.identity);
			splinePoint.transform.parent = SplineRoot.transform;
			splinePoint.name = "Point [" + pointCount + "]";
			pointCount += 1f;

		}
	}
}
