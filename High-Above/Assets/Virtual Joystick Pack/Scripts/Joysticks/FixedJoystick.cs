﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FixedJoystick : Joystick
{
    Vector2 joystickPosition = Vector2.zero;
    private Camera cam = new Camera();

   

    void Start()
    {
        //joystickPosition = RectTransformUtility.WorldToScreenPoint(cam, background.position);
        getJoyStickPos();
    }

    public override void OnDrag(PointerEventData eventData)
    {
        getJoyStickPos();
        Vector2 direction = eventData.position - joystickPosition;
        inputVector = (direction.magnitude > background.sizeDelta.x / 2f) ? direction.normalized : direction / (background.sizeDelta.x / 2f);
        ClampJoystick();
        handle.anchoredPosition = (inputVector * background.sizeDelta.x / 2f) * handleLimit;

        /*Vector2 direction = eventData.position - joystickPosition;
        inputVector = (direction.magnitude > background.sizeDelta.x / 2f) ? direction.normalized : direction / (background.sizeDelta.x / 2f);
        ClampJoystick();
        handle.anchoredPosition = (inputVector * background.sizeDelta.x / 2f) * handleLimit;*/
    }

    void getJoyStickPos() {
        joystickPosition = RectTransformUtility.WorldToScreenPoint(cam, background.position);
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
      
        OnDrag(eventData);

       
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        inputVector = Vector2.zero;
        handle.anchoredPosition = Vector2.zero;
    }


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            this.gameObject.GetComponent<Image>().enabled = true;
            Transform handle = this.gameObject.transform.GetChild(0);
            Image handleImage = handle.GetComponent<Image>();
            handleImage.enabled = true;
            this.gameObject.transform.position = Input.mousePosition;
           
            
        }
        
        if (Input.GetMouseButtonUp(0))
        {

            this.gameObject.GetComponent<Image>().enabled = false;
            Transform handle = this.gameObject.transform.GetChild(0);
            Image handleImage = handle.GetComponent<Image>();
            handleImage.enabled = false;
        }

    }

}